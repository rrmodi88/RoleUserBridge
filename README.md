RoleUserBridge
===========================

ZfcUser/BjyAuthorize Bridge for auto-adding User to the user_role_table from BjyAuthorize

Requires: ZF2 latest master,
          ZfcUser,
          BjyAuthorize

Installation:

- Install ZfcUser and BjyAuthorize as shown on thier pages
- Copy the ZfcUser/BjyAuthorize Bridge into your module or vendor directorie or use composer.
- add a entry to your application.config.php with 'RoleUserBridge'

Comments:

The bridge works actually only with an standard user after registration with ZfcUser.
At this moment there is no administration backend to change the role of an User after 
registration (only with DB-Tools). If you need an admin you need to edit manually your
user_role_linker table.

To use composer add 

"repositories": [
        {
            "type": "vcs",
            "url": "http://github.com/darkmatus/RoleUserBridge"
        }
    ],
    "minimum-stability": "dev",
	
and

	"Darkmatus/RoleUserBridge": "dev-master",
	
to your composer.json

eg,
	
<pre>	
{
    "name": "zendframework/skeleton-application",
    "description": "Skeleton Application for ZF2",
    "license": "BSD-3-Clause",
    "keywords": [
        "framework",
        "zf2"
    ],
    "repositories": [
        {
            "type": "vcs",
            "url": "http://github.com/Darkmatus/RoleUserBridge"
        }
    ],
    "minimum-stability": "dev",
    "homepage": "http://framework.zend.com/",
    "require": {
        "php": ">=5.3.3",
        "zendframework/zendframework": "2.0.4",
        "Darkmatus/RoleUserBridge": "dev-master",
        "socalnick/scn-social-auth": "dev-master"
    }
}
</pre>	
	